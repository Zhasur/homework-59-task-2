import React from 'react';
import './ChangeBtn.css'

const ChangeBtn = (props) => {
    return (
        <div>
            <button className="change-joke" onClick={props.change}>Change joke</button>
        </div>
    );
};

export default ChangeBtn;