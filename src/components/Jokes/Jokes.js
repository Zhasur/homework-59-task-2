import React from 'react';
import './Jokes.css'

const Jokes = (props) => {
    return (
        <div className="joke-block">
            <h1 className="joke">{props.joke}</h1>
        </div>
    );
};

export default Jokes;