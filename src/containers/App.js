import React, { Component } from 'react';
import './App.css';
import Jokers from "../components/Jokes/Jokes";
import ChangeBtn from "../components/ChangeBtn/ChangeBtn";

class App extends Component {

    state = {
        joke: '',
    };

    componentDidMount() {
        fetch('https://api.chucknorris.io/jokes/random').then(response => {
            if (response.ok) {
                return response.json()
            }

            throw new Error ('There is some error occurred!')
        }).then(success => {
            this.setState({joke: success.value})
        })

    }

    render() {
        return (
            <div className="App">
              <h2 className="main-title">Want some jokes?</h2>
                <ChangeBtn change={() => this.componentDidMount()}/>
                <Jokers joke={this.state.joke} />
            </div>
        );
    }
}

export default App;
